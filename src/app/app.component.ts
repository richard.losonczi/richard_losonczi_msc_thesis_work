import { Component, AfterViewInit, OnChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import {debounceTime} from 'rxjs/operators';

declare const top: any;
type NodeTypes =
  'gene' |
  'pathway' |
  'target' |
  'disease';
  
  @Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
  })
  export class AppComponent implements AfterViewInit {
  readonly limit = 20000;
  title = 'Richard-Losonczi-Thesis';
  graph: { nodes: Node[]; edges: Edge[]; } = {nodes: [], edges: []};
  types: NodeTypes[] = ['gene', 'disease', 'pathway', 'target'];
  filters: Filter[] = [{value: '', type: 'disease'}];
  filterSubject = new Subject<Filter>()
  sigma: any;
  nodesForFilter: Node[];
  edgesForFilter: Edge[];
  showSupervisorConfig = false;

  layoutTime = 1000;
  supervisor: Supervisor;

  supervisorConfig: SupervisorConfig;
  originalSupervisorConfig: SupervisorConfig;

  constructor(private http: HttpClient) {
    //(<any>top).sigma.parsers.json('assets/test.json', {container: 'container'});
    //(<any>top).sigma.parsers.json(nodes, {container: 'container'});
    this.supervisorConfig = {
      gravity: 10,
      strongGravityMode: false,
      iterationsPerRender: 1,
      adjustSizes: false,
      linLogMode: false,
      outboundAttractionDistribution: false,
      edgeWeightInfluence: 0,
      scalingRatio: 1,
      barnesHutOptimize: false,
      barnesHutTheta: 0.5,
      slowDown: 10,
      startingIterations: 1,
    };
    this.originalSupervisorConfig = {...this.supervisorConfig};
  }

  private loadData() {
    this.filterSubject.pipe(debounceTime(1000)).subscribe(filter => this.filterChange(filter))
    this.loadNodes('assets/pathway.json', '#fdf72c'); // yellow
    //this.loadNodes('assets/disease.json', '#ff4df2'); // magenta
    this.loadNodes('assets/disease.json', '#0000FF');// blue
    this.loadNodes('assets/gene.json', '#00ffff'); // bright blue
    this.loadNodes('assets/target.json', '#32fd2c'); // bright green

    // Observable
    this.loadEdges('assets/gene_disease_score.json', '#000000');
    this.loadEdges('assets/gene_target.json', '#000000');
    this.loadEdges('assets/gene_pathway.json', '#000000');

    top.graph = this.graph;
  }

  keywordChange(val) {
    this.filterSubject.next(val);
  }
  private loadEdges(url: string, color: string) {
    this.http.get(url).subscribe((data: any[]) => {
      data = data.map((edge, i) => {
        const newEdge: any = {};
        newEdge.source = edge.gene_id;
        if (edge.disease_id) {
          newEdge.target = edge.disease_id;

          if (edge.score > 0) {
            if (1-Math.log10(edge.score) >= 4.265293162) { //waterfall
              newEdge.size  = 1-Math.log10(edge.score)
              newEdge.color = '#FF0000'; //red
            }
            else if (1-Math.log10(edge.score) >= 3.578647206) {
              newEdge.size  = 1-Math.log10(edge.score)
              newEdge.color = '#FFA500'; //orange
            }
            else { // lower then 3.578647206

              newEdge.size  = 1-Math.log10(edge.score)
              newEdge.color = '#5EE527'; //green 
            }
            } else {
              newEdge.size = 1-Math.log10(1-1/1000); // size will be 1
              newEdge.color = '#5EE527'; //green 
          };
        } else if (edge.pathway_id) {
          newEdge.target = edge.pathway_id;
          //newEdge.color = '#FFFFFF'; //white
          newEdge.color = '#000000'; //black
        } else {
          newEdge.target = edge.target_id;
          //newEdge.color = '#FFFFFF'; //white 
          newEdge.color = '#000000'; //black 
        }
        newEdge.id = newEdge.source + '_' + newEdge.target + i;
        //newEdge.size = -1 *(Math.log10(edge.score))
        // newEdge.size = Math.pow((-1 *(Math.log10(edge.score))),4)
         
        newEdge.label = (1-Math.log10(edge.score)).toFixed(3);
        //newEdge.type = 'curve'
        return newEdge;
      });
      this.graph.edges = [...this.graph.edges , ...data];
    });
  }

  removeFilter(filter: Filter) {
    if (this.filters.length > 1) {
      const i = this.filters.indexOf(filter);
      this.filters.splice(i, 1);
      this.filterChain();
    }
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  private loadNodes(url: string, color: string): Observable<any> {
    const nodes = {
      nodes: null
    }
    const get = this.http.get(url);
    get.subscribe((data: any[]) => {
      const temp:any = {}
      data.forEach(node => {
        let key = '';
        if (node.disease_id) {
          key = node.disease_id;
        } else if (node.gene_id) {
          key = node.gene_id;
        } else if (node.pathway_id) {
          key = node.pathway_id;
        } else if (node.target_id) {
          key = node.target_id;
        }
        temp[key] = node;
      })
      data = [];
      Object.keys(temp).forEach(key => data.push(temp[key]));
      data = data.map((node, i) => {
        const newNode: Node = {label: '', id: '', color: '', x: 0, y: 0, size: 0};
        if (node.disease_id) {
          newNode.id = node.disease_id;
          newNode.kind = 'disease';
        } else if (node.gene_id) {
          newNode.id = node.gene_id;
          newNode.kind = 'gene';
        } else if (node.pathway_id) {
          newNode.id = node.pathway_id;
          newNode.kind = 'pathway';
        } else if (node.target_id) {
          newNode.id = node.target_id;
          newNode.kind = 'target';
        }
        if (node.title) {
          newNode.label = node.title;
        } else if (node.label) {
          newNode.label = node.label;
        }
        if (node.description) {
          newNode.label += '\n' + node.description;
        }
        
        if ((node.pathway_id)||(node.title)) {
          newNode.label = node.title
        }
        
        newNode.x = Math.random() * 10;
        newNode.y = Math.random() * 10;
        newNode.size = 1;
        newNode.color = color;
        return newNode;
      });
      nodes.nodes = data;
      this.graph.nodes = [...this.graph.nodes , ...nodes.nodes];
    });
    return get;
  }

  searchStart(filter: Filter, hint?: Node) {
    if (hint) {
      filter.id = hint.id;
      filter.value = hint.label;
    }
    filter.hint = null;
    this.filterChain();
  }

  filterChange(filter: Filter) {
    if (filter.id) {
      filter.id = null;
    }
    if (filter.value && filter.value.length > 2) {
      filter.hint = this.graph.nodes.filter(this.searchForNode(filter)).sort((a, b) => {
        let returnValue = 0;
        a.label < b.label ? returnValue = -1 : returnValue = 1;
        return returnValue;
      });
    if (filter.hint.length === 0 ){
      filter.hint.push({label:'No such item is found', id: null, color:null, x:null, y:null, size:null})
    }
    }
  }

  private searchForNode(filter: Filter): (value: Node, index: number, array: Node[]) => boolean {
    return node => {
      if (filter.id) {
        return node.id === filter.id;
      } else {
        return node.kind === filter.type && node.label.toLowerCase().includes(filter.value.toLowerCase());
      }
    };
  }

  filterChain() { // TODO add start index
    this.filters.forEach((filter, i) => {
      if (i === 0) {
        this.filterChainStep(filter, <Filter>this.graph);
      } else {
        this.filterChainStep(filter, this.filters[i - 1]);
      }
    });
    this.filterAll();
  }

  filterChainStep(filter: Filter, previousFilter?: Filter) { // TODO 2. is not Filter!
    const origin = previousFilter.nodes.filter(this.searchForNode(filter));
    filter.edges = this.getEdges(origin);
    filter.nodes = this.graph.nodes.filter(n => filter.edges.filter(edge => edge.source === n.id || edge.target === n.id).length > 0);
    // this.treeLayout(filter, origin);
    // TODO includes
  }

  private getEdges(origin: Node[]) {
    return this.graph.edges.filter(edge => origin.filter(node => node.id === edge.target || node.id === edge.source).length > 0);
  }

  private setSize(node: Node) {
    node.size = this.getEdges([node]).length + 1;
  }

  filterAll() {
    this.nodesForFilter = this.filters.map(filter => filter.nodes).reduce((allNodes, newNodes) => [...allNodes, ...newNodes]);
    this.edgesForFilter = this.filters.map(filter => filter.edges).reduce((allNodes, newNodes) => [...allNodes, ...newNodes]);
    this.nodesForFilter = Array.from(new Set(this.nodesForFilter));
    this.edgesForFilter = Array.from(new Set(this.edgesForFilter));
    if (top.sigma1 && top.sigma1.kill) {
      top.sigma1.kill();
    }
    if (this.edgesForFilter.length + this.edgesForFilter.length > this.limit) {
      return;
    }
    this.sigma = new top.sigma({
      graph: {nodes: this.nodesForFilter, edges: this.edgesForFilter},
      renderer: {
        container: document.getElementById('container'),
        type: 'canvas',
        autoRescale: true
      },
      settings: {
        edgeLabelSize:'fixed',
        defaultEdgeLabelSize:15,
        //defaultEdgeLabelColor: '#FFFFFF',
        defaultEdgeLabelColor: '#000000', // black
        //defaultEdgeLabelActiveColor: '#FFFFFF',
        defaultEdgeLabelActiveColor: '#000000', //black
        zoomMax: 50,
        zoomMin: 0.05,
        enableEdgeHovering: true,
        //edgeHoverColor: 'edge',
        //defaultEdgeHoverColor: '#000',
        edgeHoverSizeRatio: 1.33,
        edgeHoverExtremities: true,
        maxNodeSize: 4

      }

    
      //container: 'container',
      //  container: document.getElementById('sigma-container'),
      //  type:'canvas'
      
      //autoRescale: true
     // settings: {
       // edgeLabelSize: 'proportional'
     // }
    });
    const dragListener = top.sigma.plugins.dragNodes(this.sigma, this.sigma.renderers[0]);
    dragListener.bind('drag', (event: ClickNodeEvent) => {
      event.data.node.moved = true;
    });
    this.sigma.bind('clickNode', (event: ClickNodeEvent) => {
      if (event.data.node.moved) {
        event.data.node.moved = false;
        return;
      }
      this.filters.push(this.convertNodeToFilter(event.data.node));
      this.filterChain();
    });
    /* this.sigma.bind('overNode', (event: ClickNodeEvent) => {
      event.data.node
    }) */
    this.supervisor = this.sigma.startForceAtlas2(this.supervisorConfig).supervisor;
    this.sigma.graph.nodes().forEach((node: Node) => {
      node.size = this.sigma.graph.adjacentEdges(node.id).length + 1;
    });
    setTimeout(() => this.sigma.stopForceAtlas2(), this.layoutTime);
    top.sigma1 = this.sigma;
  }
  convertNodeToFilter(node: Node): Filter {
    return {
      id: node.id,
      value: node.label,
      type: node.kind
    }
  }

  runConfig() {
    if (this.sigma) {
      this.sigma.startForceAtlas2(this.supervisorConfig);
    }
  }

  resetConfig() {
    this.supervisorConfig = {...this.originalSupervisorConfig};
    this.runConfig();
  }

  ngAfterViewInit(): void {
    this.loadData();
  }
}

interface Filter {
  id?: string;
  value: string;
  type: NodeTypes;
  nodes?: Node[];
  edges?: Edge[];
  hint?: Node[];
}

interface Node {
  label: string;
  id: string;
  color: string;
  x: number;
  y: number;
  size: number;
  kind?: NodeTypes;
  moved?: boolean;
}
interface Edge extends Node {
  source: string;
  target: string;
}

interface ClickNodeEvent {
  data: {
    node: Node;
  }
}

interface Supervisor {
  start: () => void;
  stop: () => void;
  running: boolean;
  config: SupervisorConfig;
}

interface SupervisorConfig {
  linLogMode: boolean; // false: switch ForceAtlas' model from lin-lin to lin-log (tribute to Andreas Noack). Makes clusters more tight.
  outboundAttractionDistribution: boolean; // false
  adjustSizes: boolean; // false
  edgeWeightInfluence: number; // 0: how much influence you give to the edges weight. 0 is "no influence" and 1 is "normal".
  scalingRatio: number; // 1: how much repulsion you want. More makes a more sparse graph.
  strongGravityMode: boolean; // false
  gravity: number; // 1: attracts nodes to the center. Prevents islands from drifting away.
  // tslint:disable-next-line: max-line-length
  barnesHutOptimize: boolean; // true: should we use the algorithm's Barnes-Hut to improve repulsion's scalability (O(n²) to O(nlog(n)))? This is useful for large: graph but harmful to small ones.
  barnesHutTheta: number; // 0.5
  slowDown: number; // 1
  startingIterations: number; // 1: number of iterations to be run before the first render.
  iterationsPerRender: number; // 1: number of iterations to be run before each render.
}
